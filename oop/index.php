<?php
    require_once("animal.php");
    require_once("ape.php");
    require_once("frog.php");

    echo "<h2> Nama-nama Binatang </h2>";

        // Realese 1

    $sheep = new Animal("Shaun");
    echo "Nama Binatang : " . $sheep->namaBinantang . "<br>";
    echo "Jumlah Kaki : " . $sheep->legs . "<br>";
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br>";
    echo "<br>";

    $ape = new Ape("Kera Sakti");
    echo "Nama Binatang : " . $ape->namaBinantang . "<br>";
    echo "Jumlah Kaki : " . $ape->legs . "<br>";
    echo "Cold Blooded : " . $ape->cold_blooded . "<br>";
    echo "Teriak : " . $ape->yell . "<br>";
    echo "<br>";

    $frog = new Frog("Buduk");
    echo "Nama Binatang : " . $frog->namaBinantang . "<br>";
    echo "Jumlah Kaki : " . $frog->legs . "<br>";
    echo "Cold Blooded : " . $frog->cold_blooded . "<br>";
    echo "Lompat : " . $frog->jump . "<br>";

?>