<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup(){
        return view('signup');
    }

    public function register(Request $request){
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
        $jenisKelamin = $request['gender'];
        $warnaNegara = $request['nationality'];
        $bahasa = $request['language'];
        $biodata = $request['bio'];

        // dd($request);

        return view('home', ['namaDepan'=> $namaDepan, 'namaBelakang'=> $namaBelakang]);
    }
}
