@extends('layout.master')
@section('title', 'All Cast')

@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Add Cast</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $keys=>$cast)
        <tr>
            <th scope="row">{{$keys+1}}</th>
            <td>{{$cast->nama}}</td>
            <td>{{$cast->umur}}</td>
            <td>
                <form action="/cast/{{$cast->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Update</a>
                  <button type="submit" class="btn btn-danger bnt-sm">Delete</button>
                </form>
            </td>
          </tr>
        @empty
        <tr>
            <td> Cast Kosong!</td>
        </tr>
        @endforelse     
    </tbody>
  </table>

@endsection

