@extends('layout.master')

@section('title', 'Sign Up SanberBook')

@section('content')
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>

<form action="/register" method="POST">
    @csrf
    <label>First Name :</label> <br>
    <input type="text" name="fname"><br><br>

    <label>Last Name :</label> <br>
    <input type="text" name="lname"><br><br>

    <label>Gender :</label><br>
    <input type="radio" name="gender" value="1G" id="1"> <label for="1">Male</label><br>
    <input type="radio" name="gender" value="2G" id="2"> <label for="2">Female</label><br>
    <input type="radio" name="gender" value="3G" id="3"> <label for="3">Other</label><br><br>

    <label>Nationality :</label> <br>
    <select name="nationality">
        <option value="1N">Indonesian</option>
        <option value="2N">Malaysian</option>
        <option value="3N">Singaporean</option>
        <option value="4N">Other</option>
    </select> <br><br>

    <label>Language Spoken :</label> <br>
    <input type="checkbox" name="language" value="LS1" id="language1">Bahasa Indonesia <br>
    <input type="checkbox" name="language" value="LS2" id="language2">English <br>
    <input type="checkbox" name="language" value="LS3" id="language3">Other <br> <br>

    <label>Bio:</label> <br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea> <br> <br>

    <input type="submit" value="Sign Up">
</form>
@endsection

{{-- 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up SanberBook</title>
</head>
<body>

    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/register" method="POST">
        @csrf
        <label>First Name :</label> <br>
        <input type="text" name="fname"><br><br>

        <label>Last Name :</label> <br>
        <input type="text" name="lname"><br><br>

        <label>Gender :</label><br>
        <input type="radio" name="gender" value="1G" id="1"> <label for="1">Male</label><br>
        <input type="radio" name="gender" value="2G" id="2"> <label for="2">Female</label><br>
        <input type="radio" name="gender" value="3G" id="3"> <label for="3">Other</label><br><br>

        <label>Nationality :</label> <br>
        <select name="nationality">
            <option value="1N">Indonesian</option>
            <option value="2N">Malaysian</option>
            <option value="3N">Singaporean</option>
            <option value="4N">Other</option>
        </select> <br><br>

        <label>Language Spoken :</label> <br>
        <input type="checkbox" name="language" value="LS1" id="language1">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="LS2" id="language2">English <br>
        <input type="checkbox" name="language" value="LS3" id="language3">Other <br> <br>

        <label>Bio:</label> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br> <br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html> --}}